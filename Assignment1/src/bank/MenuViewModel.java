package bank;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
/**
 * This class holds the functionality that runs behind the scenes of the view.
 * Essentially anything viewed in the view class is operated via the viewmodel.
 * 
 * @author Mason McCullough
 *
 */
public class MenuViewModel implements Serializable {

	private static final long serialVersionUID = 2L;

	public void createAccount(String userOption, Scanner userInput) {
		Random ran = new Random();
		System.out.println("What is your name?");
		String bankAccountName = userInput.next();
		BankAccount bankAccount = new BankAccount(bankAccountName, ran.nextInt(10000000) , 0);
		System.out.println("Your Bank Account ID is: " + bankAccount.getAccountNumber());	

		saveAccount(bankAccount);
	}

	public void viewExistingAccount(Scanner userInput) {
		System.out.println("What is the ID of the account you would like to view?");

		while (!userInput.hasNextInt()) {
		    System.out.println("That's not an ID!");
		    userInput.next();
		}
		
		int bankAccountID = userInput.nextInt();

		Map<Integer, BankAccount> accounts = loadAccounts();
		
		if(accounts.get(bankAccountID)!=null) {

			BankAccount bankAccount = accounts.get(bankAccountID);
			System.out.println(bankAccount.toString());
		}
		else {
			System.out.println("Account does not exist.");

		}
	}

	public void depositToAccount(Scanner userInput) {
		System.out.println("What is the ID of the account you would like to deposit to?");
		
		while (!userInput.hasNextInt()) {
		    System.out.println("That's not an ID!");
		    userInput.next();
		}
		
		int bankAccountID = userInput.nextInt();

		Map<Integer, BankAccount> accounts = loadAccounts();
		if(accounts.get(bankAccountID)!=null) {
			BankAccount bankAccount = accounts.get(bankAccountID);
	
			System.out.println("How much would you like to deposit into your account?");
			while (!userInput.hasNextDouble()) {
			    System.out.println("That's not an amount!");
			    userInput.next();
			}
			
			double depositAmt = userInput.nextDouble();
			bankAccount.deposit(depositAmt);
			saveAccount(bankAccount);
		}
		else {
			System.out.println("Account does not exist.");
		}
	}

	public void withdrawFromAccount(Scanner userInput) {
		System.out.println("What is the ID of the account you would like to withdraw?");
		
		while (!userInput.hasNextInt()) {
		    System.out.println("That's not an ID!");
		    userInput.next();
		}
		
		int bankAccountID = userInput.nextInt();

		Map<Integer, BankAccount> accounts = loadAccounts();
		if(accounts.get(bankAccountID)!=null) {
			BankAccount bankAccount = accounts.get(bankAccountID);

			System.out.println("How much would you like to withdraw from your account?");
			
			while (!userInput.hasNextDouble()) {
			    System.out.println("That's not an amount!");
			    userInput.next();
			}
			
			double withdrawAmt = userInput.nextDouble();
			double balance = bankAccount.getBalance();
			if(withdrawAmt <= balance) {
				bankAccount.withdraw(withdrawAmt);
				saveAccount(bankAccount);
			} else {
				System.out.println("Not enough funds to withdraw that amount.");
			}
		}
		else {
			System.out.println("Account does not exist.");

		}
		
		
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, BankAccount> loadAccounts() {

		Map<Integer, BankAccount> accounts = new HashMap<>();

		try {
			FileInputStream filein = new FileInputStream("C://accounts.ser");
			ObjectInputStream in = new ObjectInputStream(filein);
			accounts = (Map<Integer, BankAccount>) in.readObject();
			filein.close();
			in.close();
			return accounts;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return accounts;
	}

	public void saveAccount(BankAccount bankAccount) {

		Map<Integer, BankAccount> accounts = loadAccounts();

		int accountNumber = bankAccount.getAccountNumber();
		accounts.put(accountNumber, bankAccount);

		FileOutputStream fileout;

		try {
			fileout = new FileOutputStream("C://accounts.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileout);
			out.writeObject(accounts);
			out.close();
			fileout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}								
	}

}
