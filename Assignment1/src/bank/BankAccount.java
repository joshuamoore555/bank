package bank;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

public class BankAccount implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String name;
	private int accountNumber;
	private double balance;
	private ArrayList<Transaction> transactions = new ArrayList<>();
	
	public BankAccount(String name, int accountNumber, double balance) {
		this.name = name;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("###.##");
		
		String balance = "\n#################################################################################################\nAccount Name: " + getName() + 
				"\n\nBalance: �" + df.format(getBalance()) + "";
		
		sb.append(balance + "\n\nTransactions:");
		
		ArrayList<Transaction> transactions = getTransaction();
		
		for(Transaction transaction : transactions) {
			sb.append("\n" + transaction.toString());
		}
		
		sb.append( "\n\n#################################################################################################");
		
		return sb.toString();
	}
	
	public void deposit(double amount) {
		balance += amount;
		Transaction transaction = new Transaction("DEPOSIT", amount, new Date());
		saveTransaction(transaction);
	}
	
	public void withdraw(double amount) {
		balance -= amount;
		Transaction transaction = new Transaction("WITHDRAW", amount, new Date());
		saveTransaction(transaction);
	}
	
	public void saveTransaction(Transaction transaction) {
		transactions.add(transaction);
	}
	
	public ArrayList<Transaction> getTransaction() {
		return transactions;
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
