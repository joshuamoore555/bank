package bank;

import java.io.Serializable;
import java.util.Scanner;

public class MenuView implements Serializable {

	private static final long serialVersionUID = 1L;
	private Scanner userInput;
	private boolean isRunning;
	private MenuViewModel mainMenu;

	public MenuView() {
		mainMenu = new MenuViewModel();
		isRunning = true;
		userInput = new Scanner(System.in);
		takeUserAction(userInput);

	}

	public void startupPrompt() {
		System.out.println("\nCreate a new account: type c");
		System.out.println("View an existing account: type v");
		System.out.println("Make a deposit: d");
		System.out.println("Make a withdrawal: w");
		System.out.println("Quit the program: type q\n");	
	}

	public void takeUserAction(Scanner userInput) {

		while(isRunning) {
			startupPrompt();
			String userOption = userInput.next();
			switch(userOption) {
			case "c": 	
				mainMenu.createAccount(userOption, userInput);
				break;
			case "v": 
				mainMenu.viewExistingAccount(userInput);
				break;
			case "d": 
				mainMenu.depositToAccount(userInput);
				break;
			case "w": 
				mainMenu.withdrawFromAccount(userInput);
				break;
			case "q": 
				// Quit execution
				System.out.println("Successfully Quit Application");
				isRunning = false;
				System.exit(0); // Exit with exit code 0.
				break;
			default: 
				System.err.println("\nWARNING: Invalid command entered, please refer to the menu options.\n");
				break;
			}
		}		

	}
	public static void run() {
		MenuView runMenu = new MenuView();
	}

}

