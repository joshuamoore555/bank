package bank;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class Transaction implements Serializable {

	private static final long serialVersionUID = 1L;
	private String transactionType;
	private double amount; 
	private Date date;
	
	public Transaction(String transactionType, double amount, Date date) {
		this.transactionType = transactionType;
		this.amount = amount;
		this.date = date;
	}
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("###.##");
		return "[Transaction Type = " + transactionType + ", amount = �" + df.format(amount) + ", Transaction Date = " + date + "]";
	}


	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
