package bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class Bank {

	@SuppressWarnings("unchecked")
	public Map<Integer, BankAccount> loadAccounts() {
		
		Map<Integer, BankAccount> accounts = new HashMap<>();
		try {
			File accountsFile = new File("C://accounts.ser");
			accountsFile.createNewFile();
			
			FileInputStream filein = new FileInputStream(accountsFile);
			ObjectInputStream in = new ObjectInputStream(filein);
			accounts = (Map<Integer, BankAccount>) in.readObject();
			filein.close();
			in.close();
			return accounts;
			
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return accounts;
	}
	
	public void saveAccount(BankAccount bankAccount) {

		Map<Integer, BankAccount> accounts = loadAccounts();
		
		int accountNumber = bankAccount.getAccountNumber();
		accounts.put(accountNumber, bankAccount);
			
		FileOutputStream fileout;
		File accountsFile = new File("C://accounts.ser");
	
		try {
				accountsFile.createNewFile();
				fileout = new FileOutputStream(accountsFile);
				ObjectOutputStream out = new ObjectOutputStream(fileout);
				out.writeObject(accounts);
				out.close();
				fileout.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}								
	}
	
}
